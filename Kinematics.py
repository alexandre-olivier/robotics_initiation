import math


l1 = 52
l2 = 66
l3 = 130
theta1 = float(input("Saisissez Theta1 : "))
theta_2 = float(input("Saisissez Theta2 : "))
theta_3 = float(input("Saisissez Theta3 : "))
 
#Conversion des degrés en radians /!\
theta1 = theta1*math.pi/180
theta_2 = theta_2*math.pi/180
theta_3 = theta_3*math.pi/180

Theta2c= -14*math.pi/180
Theta3c= Theta2c-(46*math.pi/180)

theta2 = theta_2-Theta2c
theta3 = theta_3-Theta3c

x3= math.cos(theta1)*(l1+l2*math.cos(theta2)+l3*math.cos(theta2+theta3))
y3= math.sin(theta1)*(l1+l2*math.cos(theta2)+l3*math.cos(theta2+theta3))
z3= -(l2*math.sin(theta2)+l3*math.sin(theta2+theta3))

print("Valeur de x3 : ", x3)
print("Valeur de y3 : ", y3)
print("Valeur de z3 : ", z3)
print("P3 : " ,x3,";", y3,";", z3)
print("---")

#-----------------------------------------#

Theta2c= -14
Theta3c= Theta2c-46

d13 = math.sqrt(x3*x3+y3*y3)-l1
theta1= math.acos(x3/(d13+l1))*180/math.pi #convertit en degres

d=math.sqrt(z3*z3+d13*d13)

a=math.asin(z3/d)*180/math.pi
b= (math.acos((l2*l2+d*d-l3*l3)/(2*d*l2)))*180/math.pi

theta4= (math.acos((l2*l2+l3*l3-d*d)/(2*l2*l3)))*180/math.pi

theta3=180-theta4+Theta3c
theta2= -(a-b-Theta2c)

print("Valeur de theta1 : ", theta1)
print("Valeur de theta2 : ", theta2)
print("Valeur de theta3 : ", theta3)
print("Valeur de a : ", a)
print("Valeur de b : ", b)