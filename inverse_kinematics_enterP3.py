import math

L1= 51
L2= 63.7
L3= 93

X3=float(input("Entrez X3 : "))
Y3=float(input("Entrez Y3 : "))
Z3=float(input("Entrez Z3 : "))

Theta2c= -20.69
Theta3c= 90+Theta2c-5.06

d13 = math.sqrt(X3*X3+Y3*Y3)-L1
theta1= math.acos(X3/(d13+L1))*180/math.pi #convertit en degres

d=math.sqrt(Z3*Z3+d13*d13)

a=math.asin(Z3/d)*180/math.pi
b= (math.acos((L2*L2+d*d-L3*L3)/(2*d*L2)))*180/math.pi

theta4= (math.acos((L2*L2+L3*L3-d*d)/(2*L2*L3)))*180/math.pi

theta3=-(180-theta4-Theta3c)
theta2= -(a+b-Theta2c)


print("Valeur de theta1 : ", theta1)
print("Valeur de theta2 : ", theta2)
print("Valeur de theta3 : ", theta3)
print("Valeur de a : ", a)
print("Valeur de b : ", b)