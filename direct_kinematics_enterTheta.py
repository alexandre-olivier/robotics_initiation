import math

l1 = 51
l2 = 63.7
l3 = 93
theta1 = float(input("Saisissez Theta1 : "))
theta_2 = float(input("Saisissez Theta2 : "))
theta_3 = float(input("Saisissez Theta3 : "))
 
#Conversion des degrés en radians /!\
theta1 = theta1*math.pi/180
theta_2 = theta_2*math.pi/180
theta_3 = theta_3*math.pi/180

Theta2c= -20.69*math.pi/180
Theta3c= (90*math.pi/180)+Theta2c-(5.06*math.pi/180)

theta2 = theta_2-Theta2c
theta3 = -(theta_3-Theta3c)

x3= math.cos(theta1)*(l1+l2*math.cos(theta2)+l3*math.cos(theta2+theta3))
y3= math.sin(theta1)*(l1+l2*math.cos(theta2)+l3*math.cos(theta2+theta3))
z3= -(l2*math.sin(theta2)+l3*math.sin(theta2+theta3))

print("Valeur de x3 : ", x3)
print("Valeur de y3 : ", y3)
print("Valeur de z3 : ", z3)
print("P3 : " ,x3,";", y3,";", z3)